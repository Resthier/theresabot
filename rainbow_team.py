import numpy
import sys
import r6sapi
import asyncio
import os

# The uplay ID that corresponds to a persons discord id
discordToUplay = {'2561': '7f51410f-f826-412f-968b-c8128c71f175',  # Euan
                  '9088': 'e454b266-8408-4ab2-89bd-5b0aa6c88c61',  # Ben
                  '1241': '9404cae4-fcef-4009-b504-30778f005995',  # Tomek
                  '2533': 'fea60d88-609a-4ad8-9fcd-023a89e42eb8',  # Chim
                  '8901': '89f9eeb7-2756-478e-b162-c60693836f31',  # Richard
                  '1494': '7d59bb3c-173e-41a8-ab20-fc5c22226ab8',  # Tristan
                  '8027': '7879b9ea-6a1f-4a77-931d-2dab8995c0cd',  # Pymedes
                  '9497': 'cb6a6a0f-908c-44c5-8af9-8680b218688e',  # Tom
                  '1582': 'f509e55e-03a4-4911-a073-9f96a9d5ee62',  # Wiktor
                  '9058': '177647a1-f27e-4fec-a485-70925e856d02',  # Finn
                  '6963': 'd8ddb220-5a73-46aa-8f8e-c13cf9c83178',  # Leon
                  '4093': '787d7069-2049-405d-a2df-eff15605bbb6',  # Markus
                  }

attackOps = ["sledge", "thatcher", "ash", "thermite", "twitch", "montagne", "glaz", "fuze", "blitz",
             "iq", "buck", "blackbeard", "capitao", "hibana", "jackal", "ying", "zofia", "dokkaebi",
             "lion", "finka", "maverick", "nomad", "iana", "gridlock"]

defenseOps = ["smoke", "mute", "castle", "pulse", "doc", "rook", "kapkan", "tachanka", "jager", "bandit",
              "frost", "valkyrie", "caveira", "echo", "mira", "lesion", "ela", "vigil", "maestro",
              "alibi", "clash", "kaid", "mozzie", "wamai", "clash", "warden"]


@asyncio.coroutine
def run(people):
    secrets = open(os.path.dirname(__file__) + '/../secrets.txt', 'r')
    keys = secrets.readlines()
    secrets.close()
    keys = [item[:-1] for item in keys]  # remove \n

    playerDict = {}

    auth = r6sapi.Auth(keys[4], keys[5])
    for person in people:
        player = yield from auth.get_player(platform=r6sapi.Platforms.UPLAY, uid=person.ID)
        yield from player.check_general()
        yield from player.get_all_operators()
        yield from player.check_queues()
        playerDict[person] = [player.name, player.time_played, player.operators,
                              player.ranked.kills, player.ranked.deaths]

    auth.close()
    return playerDict


class TeamWeight(object):
    def __init__(self, team):
        # Initialises the team weight class
        self.team = team  # the array containing the subclasses for each player
        self.team1 = []
        self.team2 = []
        self.value = 0

    def getTeam(self):
        return self.team

    def setTeam(self, team):
        self.team = team

    def organiseTeam(self):
        # fits the teams based on their KD score
        highest = self.team[0]
        temp = self.team
        # sort by KD
        length = len(temp)
        while True:
            for counter in range(0, length - 1):
                if temp[counter].weighted_KD > temp[counter + 1].weighted_KD:
                    # swap
                    temp2 = temp[counter]
                    temp[counter] = temp[counter + 1]
                    temp[counter + 1] = temp2
                # end if
            length -= 1
            if length == 0:
                break
        self.team = temp
        print(temp)
        for person in temp:
            print(person)

        # passing the team array back to the class - overwrites it with the sorted array
        for counter in range(round(len(temp) / 2)):
            self.team1.append(temp[0])
            self.team.pop(0)
            if len(self.team) == 0:
                break
            self.team2.append(temp[0])
            self.team.pop(0)

        self.value, pos = self.swipSwap(2)
        print(self.value)

        # Only runs if there are values in the pos list
        if pos:
            self.team1[pos[0]], self.team2[pos[1]] = self.team2[pos[1]], self.team1[pos[0]]


        # Recursive algorithm to make teams more equal based on the sum of the players weighted KDs
        # depth 2 (See the outcome of two swaps), whichever path leads to the smallest difference between
        # the weighted KDs is chosen

    def swipSwap(self, depth, currMinWeight="DEFAULT", currSwap=[]):
        print("Depth: " + str(depth))
        if currMinWeight == "DEFAULT":
            currMinWeight = sum(player.weighted_KD for player in self.team1) - sum(player.weighted_KD for player in self.team2)
            print("Starting Value" + str(currMinWeight))
        for i in range(len(self.team1)):
            for j in range(len(self.team2)):
                self.team1[i], self.team2[j] = self.team2[j], self.team1[i]
                diffWeight = sum(player.weighted_KD for player in self.team1) - sum(player.weighted_KD for player in self.team2)
                print("This Difference: " + str(diffWeight))
                if abs(diffWeight) < abs(currMinWeight):
                    currSwap = [i, j]
                    currMinWeight = diffWeight
                    print("Curr Min: " + str(currMinWeight))
                if depth == 2:
                    currMinWeight, currSwap = self.swipSwap(depth - 1, currMinWeight, currSwap)
                self.team1[i], self.team2[j] = self.team2[j], self.team1[i]

        return currMinWeight, currSwap


    def fittingTeams(self):
        # The point of this function is to run and correct the teams made by the organise teams function.
        # The function doesn't require any variables nad returns a true value when changes
        # were made and false when there weren't any


        # obtain data to analyse
        team1_attack = []
        team1_defence = []

        team2_attack = []
        team2_defence = []

        for person in self.team1:
            team1_attack.append(person.KD_attack)
            team1_defence.append(person.KD_defence)

        for person in self.team2:
            team2_attack += person.KD_attack
            team2_defence += person.KD_defence

        team1_attack = numpy.array(team1_attack)
        team1_defence = numpy.array(team1_defence)
        team2_attack = numpy.array(team2_attack)
        team2_defence = numpy.array(team2_defence)

        # find means
        data = [] # this array stores the data in this order team1 and team2 means followed by team 1 & 2 standard deviations

        data.append(numpy.mean(team1_attack))   # 0
        data.append(numpy.mean(team1_defence))  # 1
        data.append(numpy.mean(team2_attack))   # 2
        data.append(numpy.mean(team2_defence))  # 3

        # find standard deviations

        data.append(numpy.std(team1_attack))    # 4
        data.append(numpy.std(team1_defence))   # 5
        data.append(numpy.std(team2_attack))    # 6
        data.append(numpy.std(team2_defence))   # 7


        # Check if the teams are consistent within 2 std of each other.
        # First check is for attack and the second check is for defence
        for i in range(0,1,1):
            if numpy.abs(data[i] - data[i+1]) <= 3*numpy.abs(data[i+4] - data[i+5]):
                consistent = True
            else:
                consistent = False
        # If the teams are all good then return True
        if consistent:
            return True

        # The rest runs only if the teams are not consistent

        # Check for any players having an attack/defence bias
        BiasList = []
        for player in self.team1:
            if numpy.abs(player.KD_attack - data[0]) > 2*data[4]:
                BiasList.append([player,"A",1])
            if numpy.abs(player.KD_defence - data[1]) > 2*data[5]:
                BiasList.append([player,"D",1])

        for player in self.team2:
            if numpy.abs(player.KD_attack - data[2]) > 2*data[6]:
                BiasList.append([player,"A",2])
            if numpy.abs(player.KD_defence - data[3]) > 2*data[7]:
                BiasList.append([player,"D",2])

        if len(BiasList) <= 1:
            # return True if the list of Bias players is empty or contains one player
            return True

        BiasListAttack = []
        BiasListDefence = []

        for player in BiasList:
            if player[1] == "A":
                BiasListAttack.append(player)
            else:
                BiasListDefence.append(player)






class Player(object):
    def __init__(self, ID, name, KD_attack=0, KD_defence=0, KD_ranked=0, hours=0):
        self.ID = ID
        self.KD_attack = KD_attack
        self.KD_defence = KD_defence
        self.KD_ranked = KD_ranked
        self.hours = hours
        self.weighted_KD = 0
        self.name = name
        print(ID)

    def genStats(self, stats):
        print("here")
        KD_ranked = stats[3] / stats[4]
        self.hours = stats[1] / (60 * 60)
        self.scanOperators(stats[2])
        print("Name: {0}, KD Attack: {1}, KD Defense: {2}, KD Ranked: {3}, Hours: {4}".format(
            stats[0], self.KD_attack, self.KD_defence, KD_ranked, self.hours))

    # scan through all the operators for each side and obtain a KD
    def scanOperators(self, operators):
        playerAttackOps = []
        playerDefenseOps = []

        # ops is a list containing all the values in the operators dictionary
        ops = list(operators.get(op) for op in operators)
        for op in ops:
            if op.name in attackOps:
                playerAttackOps.append(op)
            elif op.name in defenseOps:
                playerDefenseOps.append(op)

        attackKills = sum(op.kills for op in playerAttackOps)
        attackDeaths = sum(op.deaths for op in playerAttackOps)
        defenseKills = sum(op.kills for op in playerDefenseOps)
        defenseDeaths = sum(op.deaths for op in playerDefenseOps)

        print(attackKills)
        print(attackDeaths)
        print(defenseKills)
        print(defenseDeaths)

        self.KD_attack = attackKills / attackDeaths
        self.KD_defence = defenseKills / defenseDeaths

        self.weightedKD()

    def weightedKD(self):
        # weighs KD giving the ranked KD a preference
        temp = self.KD_attack / 10 + self.KD_defence / 10 + self.KD_ranked
        temp /= 1.2
        self.weighted_KD = temp


# Creates a player object for each player with their uplayID and Discord name
# Creates a team weight object which creates *balanced* teams
# Returns two arrays, one for each team, containing the discord names of the players in that team
async def main(playerNames):
    # Creates an array that contains player objects with attributes of both their uplayID and their
    # discord name for each
    players = []
    for person in playerNames:
        # The dictionary that contains the uplayIDs has the four numbers at the end of one's discord
        # name as the key
        uid = discordToUplay.get(person[-4:])
        newPlayer = Player(uid, person)
        # await newPlayer.genStats()
        players.append(newPlayer)

    playerStats = await run(players)

    for person, stats in playerStats.items():
        person.genStats(stats)

    print(players)
    teamCreator = TeamWeight(players)
    teamCreator.organiseTeam()

    # Creates two arrays containing the discord names of the players
    team1 = [person.name for person in teamCreator.team1]
    team2 = [person.name for person in teamCreator.team2]

    return team1, team2, teamCreator.value
