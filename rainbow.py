import r6sapi as api
import asyncio
import aiohttp
import os
import sys

@asyncio.coroutine
def run(name, op):
    #Put the values for the reddit api and the discord bot id in secrets.txt
    secrets = open(os.path.dirname(__file__) + '/../secrets.txt', 'r')
    keys = secrets.readlines()
    secrets.close()
    keys = [item[:-1] for item in keys] #remove \n

    auth = api.Auth(keys[4], keys[5])
    player = yield from auth.get_player(name, api.Platforms.UPLAY)
    if op == 'player':
        yield from player.check_general()
        killsDeath = player.kills if player.deaths is 0 else round(player.kills/player.deaths, 2)
        accuracy = round(player.bullets_hit/(player.bullets_hit+player.bullets_fired), 2)
        infoList = [player.kills, player.time_played,
                    player.melee_kills, killsDeath, accuracy,
                    player.headshots, player.bullets_hit, player.bullets_fired]
    else:
        operator = yield from player.get_operator(op.lower())
        killsDeath = operator.kills if operator.deaths is 0 else round(operator.kills/operator.deaths, 2)
        infoList = [operator.kills, operator.time_played, operator.headshots,
                    operator.melees, killsDeath, operator.dbnos]

    auth.close()
    return infoList

async def main(name, op):
    infoList = await run(name, op)
    return infoList
